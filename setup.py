try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'version': '0.1',
    'name': 'dgsAdmin',
    'description': 'Dedicated Game Server Admin',
    'author': 'Will Moggridge (Dark X Dragon)',
    'author_email': 'will.moggridge@gmail.com',
    #'url': '',
    #'download_url': '',
    'install_requires': ['nose'],
    'packages': ['dgsadmin'],
    'scripts': []
}

